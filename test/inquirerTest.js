const inquirer = require("inquirer");

// 定义问题
let qList = [
  {
    type: "input",
    message: "请输入用户名",
    name: "username",
    validate(answer) {
      if (!answer) {
        return "当前为必填项";
      } else {
        return true;
      }
    },
  },
  {
    type: "confirm",
    message: "是否执行下载",
    name: "isDownload",
  },
  {
    type: "list",
    message: "选择下载方式",
    name: "method",
    choices: ["npm", "cnpm", "pnpm", "yarn"],
    when(preAnswer) {
      return preAnswer.isDownload;
    },
  },
  {
    type: "checkbox",
    message: "请选择要安装得组件",
    name: "components",
    choices: ["ts", "eslint", "prettier", "router"],
  },
];

inquirer.prompt(qList).then((answer) => {
  console.log("***answer", answer);
  console.log("***username", answer.username);
});
