const ora = require("ora");

const spinner = ora("正在下载...").start();

setTimeout(() => {
  // 结束等待状态的⽅式，⼀般为耗时操作如下载，这⾥使⽤ timeout 定时器模拟
  //   spinner.succeed("下载成功");
  //   spinner.fail('下载失败')
  // spinner.info('下载内容')
  spinner.warn("警告");
}, 2000);
