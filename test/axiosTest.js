const axios = require("axios");

axios({
  method: "get",
  url: "https://api.github.com/users/kirin-wu/repos",
  headers: {
    Authorization: "token " + "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",
  },
}).then((res) => {
  const { data } = res;
  console.log(res);
  let arr = data.reduce((prev, item) => {
    if (item.name.startsWith("create-")) {
      prev.push(item.name);
    }
    return prev;
  }, []);
  console.log(arr);
});
