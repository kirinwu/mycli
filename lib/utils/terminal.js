const { execFile } = require("child_process");

exports.commandExec = (...args) => {
  console.log(...args);
  return new Promise((resolve, reject) => {
    const childProcess = execFile(...args);
    // 将子进程安装进度输出到主进程
    childProcess.stdout.pipe(process.stdout);
    // 错误信息
    childProcess.stdout.pipe(process.stderr);
    // 子进程执行完毕
    childProcess.on("close", () => {
      resolve();
    });
  });
};
