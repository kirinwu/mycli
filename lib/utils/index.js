// utils/index.js
// 将传⼊路径中的 \ 统⼀处理为 /
exports.parsePath = (path) => {
  return path.replace(/\\/g, "/");
};
