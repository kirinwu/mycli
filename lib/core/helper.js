module.exports = (program) => {
  // option设定自定义选项
  program.option("-f, --framework <framework>", "select framework");
  program.option("-d, --dest <dest>", "destination folder");

  // 帮助信息处理
  // 自定义示例命令语法
  const example = {
    create: ["mycli create <project-name>"],
    config: ["mycli config set <key> [value]", "mycli config get <key>"],
  };
  program.on("--help", () => {
    console.log("Example:");
    Object.keys(example).forEach((action) => {
      example[action].forEach((item) => {
        console.log("  " + item);
      });
    });
  });
};
