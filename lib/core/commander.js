const create = require("../actions/create");
const config = require("../actions/config");

module.exports = (program) => {
  // create
  program
    .command("create <project-name> [others...]")
    .alias("c")
    .description("创建新的项目")
    .action(create);

  // config
  program
    .command("config <set|get> [other...]")
    .alias("cfg")
    .description("配置信息处理")
    .action(config);
};
