const axios = require("axios");
const inquirer = require("inquirer");
const path = require("path");
const { parsePath } = require("../utils/index");
// 默认为回调写法，为避免过多嵌套，使⽤ node ⽅法转换为 promise 操作，⽽且该库⽀持
const { promisify } = require("util");
const downloadRepo = promisify(require("download-git-repo"));
const ora = require("ora");
const fs = require("fs-extra");
const { commandExec } = require("../utils/terminal");
const chalk = require("chalk");
const ncp = promisify(require("ncp"));
const execa = require("execa");

module.exports = async (projectName, args) => {
  // 查询仓库有哪些模板使用
  const res = await axios({
    method: "get",
    url: "https://api.github.com/users/kirin-wu/repos",
    headers: {
      Authorization: "token " + "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",
    },
  });
  const { data } = res;
  const templateName = data.reduce((prev, item) => {
    if (item.name.startsWith("create-")) {
      prev.push(item.name);
    }
    return prev;
  }, []);

  // 提供列表选择模板
  let qList = [
    {
      type: "list",
      message: "请选择想要的模板",
      name: "template",
      choices: templateName,
    },
  ];

  const { template } = await inquirer.prompt(qList);

  // 生成模板缓存地址
  const tempPath = parsePath(
    process.env[process.platform === "win32" ? "USERPROFILE" : "HOME"] + "/.tmp"
  );
  const repo = "kirin-wu/" + template;
  const cachePath = path.resolve(tempPath, repo);

  // 检测是否有缓存
  if (!fs.existsSync(cachePath)) {
    // 下载仓库的模板到本地
    const spinner = ora("正在下载...").start();
    await downloadRepo(repo, cachePath);
    spinner.succeed("下载成功");
  }

  // 检测目标路径是否存在
  const dest = `./${projectName}`;
  if (fs.existsSync(dest)) {
    const isReplace = await inquirer.prompt([
      {
        type: "confirm",
        message: "目标路径存在，是否替换？",
        name: "isReplace",
      },
    ]);
    if (!isReplace) {
      console.log(chalk.red.bold("目标路径存在，不替换！"));
      return;
    }
    await fs.remove(dest);
  }

  // 把缓存目录下载到当前文件夹
  await ncp(cachePath, projectName);

  // 安装依赖
  const npm = process.platform === "win32" ? "npm" : "npm";
  const spinnerInstall = ora("正在安装依赖").start();
  await commandExec(npm, ["install"], { cwd: `./${projectName}` });
  // const { stdout, stderr } = await execa(npm, ["install"], {
  //   cwd: `./${projectName}`,
  // });
  // console.log("***stdout", stdout, stderr);
  spinnerInstall.succeed("依赖安装完成");
};
