#! /usr/bin/env node
// console.log("通过 node.js 执⾏代码");

const { program } = require("commander");
const initHelper = require("../lib/core/helper");
const commander = require("../lib/core/commander");

// helper
initHelper(program);
// commander
commander(program);

// program.parse(process.argv);
program.parse();
